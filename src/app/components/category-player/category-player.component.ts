import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router, Params} from '@angular/router';

import {CategoriesService} from '../../services/categories/categories.service';

@Component({
  selector: 'app-category-player',
  templateUrl: './category-player.component.html',
  styleUrls: ['./category-player.component.css']
})
export class CategoryPlayerComponent implements OnInit {

  public category:any = {};
  public id:any;

  constructor(private _categoriesService:CategoriesService, 
    private _activatedRouter:ActivatedRoute,
    private _router:Router) { }

  ngOnInit(): void {
    this._activatedRouter.params.subscribe((params:Params)=>{
      
      this._categoriesService.getCategoryById(params.id).subscribe(
        (res)=>{
          this.category = res;
          console.log(this.category);
        },
        (err) =>{
          console.log(err);
        }
      );
    });

    

  }

}
