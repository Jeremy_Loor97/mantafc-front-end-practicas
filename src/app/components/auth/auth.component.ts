import { Component, OnInit } from '@angular/core';
import {AuthService} from '../../services/auth/auth.service';

import {ActivatedRoute, Router, Params} from '@angular/router';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.css']
})
export class AuthComponent implements OnInit {

  public user = {
    Username:"",
    Password:""
  };

  public token:String;

  public validUser = true;
  private name = "";
  constructor(private _authService:AuthService, private _router:Router) { 
    this.token = localStorage.getItem("token");
  }

  ngOnInit(): void {  
    if(this.token != null){
      this._router.navigate(['/panel']);
    }
  }

  signIn():void{
    this._authService.signIn(this.user).subscribe(
      (res)=>{
        
        localStorage.setItem("token", res.token);
        localStorage.setItem("user", JSON.stringify(res.user));
        this._router.navigate(['/panel']);      
      },    
      (err) =>{
        this.validUser = false;
        
      }
    );
  }



}
