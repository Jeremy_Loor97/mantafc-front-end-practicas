import { Component, OnInit } from '@angular/core';
import {AuthService} from '../../services/auth/auth.service';
import {CategoriesService} from '../../services/categories/categories.service';

import Swal from 'sweetalert2/dist/sweetalert2.js';



@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.css']
})
export class NavigationComponent implements OnInit {
  public categories:any = [];
  public user:any = {};

  constructor(private _authService:AuthService, private _categoriesService:CategoriesService) { }

  ngOnInit(): void {
    this.user = JSON.parse(localStorage.getItem('user'));
    
    if(this.user.isAdmin){
      this.listOfCategoriesForAdmin();
    }else{
      this.listOfCategoriesById();
    }
  }

  listOfCategoriesById(){
    this._categoriesService.getCategoryByUser(this.user.id).subscribe(
      (res)=>{
        console.log(res);
        this.categories = res;
        
      },
      (err)=>{console.log(err)}
    );
  }

  listOfCategoriesForAdmin(){
    this._categoriesService.getCategories().subscribe(
      (res)=>{
        this.categories = res;
      },
      (err)=>{console.log(err)}
    );
  }

  loggout():void {
    Swal.fire({
      title: '¿Estás seguro?',
      text: "La sesión actual sera cerrada",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#03A9F4',
      cancelButtonColor: '#F44336',
      confirmButtonText: '<i class="zmdi zmdi-power"></i> SI, Salir!',
		  cancelButtonText: '<i class="zmdi zmdi-close-circle"></i> NO, Cancelar!'
    }).then((result)=> {
      if (result.value) {
        this._authService.loggout();
      }
    });
      
  }

}
