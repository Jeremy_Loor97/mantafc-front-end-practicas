import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {AuthService} from '../../services/auth/auth.service';

import Swal from 'sweetalert2/dist/sweetalert2.js';

@Component({
  selector: 'app-panel',
  templateUrl: './panel.component.html',
  styleUrls: ['./panel.component.css']
})
export class PanelComponent implements OnInit {
  
  public user:any;
  public isAdmin:any;

  constructor(private _router:Router, private _authService:AuthService) { 
    this.user = JSON.parse(localStorage.getItem('user'));
    
    this.isAdmin = this.user.isAdmin;
       

  }

  ngOnInit(): void {
  }

  loggout():void {
    Swal.fire({
        title: '¿Estás seguro?',
        text: "La sesión actual sera cerrada",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#03A9F4',
        cancelButtonColor: '#F44336',
        confirmButtonText: 'Si, Salir!',
        cancelButtonText: 'Cancelar!'
      }).then((result)=> {
        if (result.value) {
          this._authService.loggout();
        }
      });
      
    }


 
}
