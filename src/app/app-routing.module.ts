import { NgModule } from '@angular/core';
import { Routes, RouterModule, ActivatedRoute } from '@angular/router';

import {AuthComponent} from 'src/app/components/auth/auth.component';
import {PanelComponent} from 'src/app/components/panel/panel.component';
import {CategoryPlayerComponent} from 'src/app/components/category-player/category-player.component';

import {AuthGuard} from "../app/guard/auth.guard";

const routes: Routes = [
  {path:'', redirectTo:'/signin', pathMatch:'full'},
  {path:'signin', component:AuthComponent},
  {path:'panel', component:PanelComponent, canActivate:[AuthGuard]},
  {path:'category/:id',component:CategoryPlayerComponent, canActivate:[AuthGuard]},
  {path:'**', component:PanelComponent, canActivate:[AuthGuard]}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
 