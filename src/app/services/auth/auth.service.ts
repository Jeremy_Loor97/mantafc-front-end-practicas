import { Injectable } from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';

import { Router } from "@angular/router";


@Injectable({
  providedIn: 'root'
})
export class AuthService {

  public url:String;

  constructor(private _http:HttpClient, private _router:Router) { 
    this.url = "http://localhost:3000/api";
  }

  signIn(user):Observable<any>{
    return this._http.post(`${this.url}/auth/signin`, user);
  }

  loggedIn(): Boolean{
    return !!localStorage.getItem('token');
  }

  getToken(){
    return localStorage.getItem('token');
  }

  loggout():void{
    localStorage.clear();
    this._router.navigate(['/']);
  }
}
