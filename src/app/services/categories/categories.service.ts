import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CategoriesService {
  public url:String;
  constructor(private _http:HttpClient) {
    this.url = 'http://localhost:3000/api/categories';
  }

  getCategoryById(id):Observable<any>{
    return this._http.get(`${this.url}/${id}`);
  }

  getCategories():Observable<any>{
    return this._http.get(`${this.url}`);
  }

  getCategoryByUser(userId):Observable<any>{
    return this._http.get(`${this.url}/user/${userId}`);
  }
}
